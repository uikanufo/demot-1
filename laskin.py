# !bin/bash/python3
kirja = {
"+" : lambda a,b : a+b,
"-" : lambda a,b : a-b,
"*" : lambda a,b : a*b,
"/" : lambda a,b : a/b
}
luku = 0
while True:
    try:
        print(f"luku on {luku}.")
        operaatio = input("Anna operaatio (tyhjä lopettaa): ")
        if operaatio == "":
            break
        luku = kirja[operaatio[0]](luku, int(operaatio[1:]))
    except:
        print('En ymmärtänyt, yritä uudestaan muotoa "[+-*/]luku"')
        
